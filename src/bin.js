#!/usr/bin/env node
const path = require('path');
const HashIds = require('hashids');
require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

const hashIds = new HashIds(process.env.HASH_PHRASE, Number(process.env.HASH_PADDING));

require('yargs/yargs')(require('yargs/helpers').hideBin(process.argv))
  .command(
    'encode <value>',
    'Encode value',
    {},
    (argv) => {
      try {
        const value = String(argv.value);

        if (!/^[\d]+$/.test(value)) {
          throw new Error('Invalid value to encode.');
        }

        console.log(hashIds.encode(value));
      } catch (error) {
        console.log(error.message);
      }
    },
  )
  .command(
    'decode <value>',
    'Decode value',
    {},
    (argv) => {
      try {
        if (!/^([^\W_]*?)$/.test(argv.value)) {
          throw new Error('Invalid value to decode.');
        }

        const value = hashIds.decode(argv.value).pop();

        if (value === undefined) {
          throw new Error('Undefined decoded value.');
        }

        console.log(String(value));
      } catch (error) {
        console.log(error.message);
      }
    },
  )
  .argv;